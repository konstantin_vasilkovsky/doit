$(function(){
	var showMoreLogos = $('#show-more-logos');
		showMoreLogos.expanded = false;
	var menu = $('nav');
	var $root = $('html, body');
	var filtersExpanded = false;
	$("nav:not(.blog)").mouseenter(function(){
		menu.addClass('menu-scrolled');
	}).mouseleave(function(){
		if($(window).scrollTop() < menu.height())menu.removeClass('menu-scrolled');
	});
	$(window).bind('scroll', function () {
		$('#navbarCollapse').collapse('hide');
	    if ($(window).scrollTop() > menu.height()) {
	        menu.addClass('menu-scrolled');
	    } else {
	        menu.removeClass('menu-scrolled');
	    }
	});
	$('#uploadCV').on('click', function(){
		$('#attachedCV').trigger('click');
		$('#attachedCV').on('change', function(){
			$('#cvLabel').val(this.files[0].name);
		});
	});
	$('.scrollable a, #header a').on('click', function() {
		if($.attr(this, 'href') != '#'){
			$('#navbarCollapse').collapse('hide');
		    $root.animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top - $('.navbar').outerHeight()
		    }, 500);	
		}
	    return false;
	});

	showMoreLogos.on('click', function(){
		showMoreLogos.expanded ? $('.logos-container').removeClass('expanded') : $('.logos-container').addClass('expanded');
		showMoreLogos.expanded = !showMoreLogos.expanded;
		showMoreLogos.expanded ? showMoreLogos.text('Hide').addClass('active') : showMoreLogos.text('Show More').removeClass('active');

	});
	$('#show-more-filters').on('click', function(){
		filtersExpanded = !filtersExpanded;
		$('#filters').slideToggle('slow');
		filtersExpanded ? $(this).text('Hide Filters').addClass('active') : $(this).text('More Filters').removeClass('active');
	});
});